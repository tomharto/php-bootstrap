<?php
namespace TH\Bootstrap\View;
use TH\Bootstrap\Interfaces\IConfig;


class Renderer {

    private static $instance;
    private static $config;

    public static function init(IConfig $config){
        self::$config = $config;
        self::getInstance();
    }


    public static function getInstance(){
        if(self::$instance === null){
            self::$instance = new Renderer(self::$config);
        }

        return self::$instance;
    }

    private static $twig;

    public function __construct(IConfig $config){

        $templates = $config->get('view', 'template_paths', 'path');

        $loader = new \Twig_Loader_Filesystem();

        foreach($templates as $path){
            $loader->addPath($path);
        }

        self::$twig = new \Twig_Environment($loader, array(
            //'cache' => $config->get('view', 'cache_path'),
            'cache' => false
        ));
    }


    public static function render($file, $data = array()){
        return self::$twig->render($file, $data);
    }

}
