<?php
namespace TH\Bootstrap\Response;

use TH\Bootstrap\Interfaces\IResponse;
use TH\Bootstrap\Response\Response;
use TH\Bootstrap\View\Renderer;

class JSONResponse extends Response implements IResponse {

    protected $contentType = 'application/json';

    public function __construct($data){
        $this->setContent($data);
    }

    public function getContent(){
        return json_encode($this->content);
    }

}
