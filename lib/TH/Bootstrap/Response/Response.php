<?php
namespace TH\Bootstrap\Response;

use TH\Bootstrap\Interfaces\IResponse;

abstract class Response {

    protected $contentType = 'text/html';
    protected $content = '';

    public function setContentType($type){
        $this->contentType = $type;
    }

    public function setContent($content){
        $this->content = $content;
    }

    public function getContentType(){
        return $this->contentType;
    }

    public function getContent(){
        return $this->content;
    }

}
