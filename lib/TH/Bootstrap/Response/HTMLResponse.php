<?php
namespace TH\Bootstrap\Response;

use TH\Bootstrap\Interfaces\IResponse;
use TH\Bootstrap\Response\Response;
use TH\Bootstrap\View\Renderer;

class HTMLResponse extends Response implements IResponse {

    public function __construct($content, $data = array()){
        if(substr($content, -10) === '.twig.html'){
            $this->setContentFromFile($content, $data);
        } else {
            $this->setContent($content);
        }
    }


    private function setContentFromFile($content, $data){
        $this->setContent(Renderer::render($content, $data));
    }

}
