<?php
namespace TH\Bootstrap;

use TH\Bootstrap\Route\Router;
use TH\Bootstrap\Exception\NoRouteException;
use TH\Bootstrap\Interfaces\IResponse;

class Dispatcher {

    private $router;

    public function __construct(Router $router){
        $this->router = $router;
    }

    private function getRoute($route){
        try {
            return $this->router->findRoute($route);
        } catch (NoRouteException $ex){
            return $this->router->getErrorRoute(404);
        }
    }

    public function dispatch(){
        $uri = $_SERVER['REQUEST_URI'];
        $route = $this->getRoute($uri);

        $clz = $route->getClass();
        $action = $route->getAction();

        $class = new $clz();

        $response = $class->$action();

        $this->processResponse($response);
    }

    public function processResponse(IResponse $response){

        header('Content-Type: ' . $response->getContentType());
        echo $response->getContent();
    }

}
