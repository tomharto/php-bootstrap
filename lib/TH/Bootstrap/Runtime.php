<?php
namespace TH\Bootstrap;

use TH\Bootstrap\Interfaces\IConfig;
use TH\Bootstrap\ErrorHandler;
use TH\Bootstrap\View\Renderer;
use TH\Bootstrap\Route\Router;


class Runtime {

    public function __construct(IConfig $applicationConfig){
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        ErrorHandler::init($applicationConfig);


        Renderer::init($applicationConfig);


        $router = Router::createFromFile($applicationConfig->get('route_file'));
    }
}
