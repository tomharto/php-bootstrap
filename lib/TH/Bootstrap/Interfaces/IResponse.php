<?php
namespace TH\Bootstrap\Interfaces;

interface IResponse {

    public function getContentType();
    public function getContent();

    public function setContentType($type);
    public function setContent($content);
}
