<?php
namespace TH\Bootstrap\Interfaces;

use TH\Bootstrap\Exception\NoKeyException;


interface IConfig {

    public function get(...$key);
}
