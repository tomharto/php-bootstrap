<?php
namespace TH\Bootstrap;
use TH\Bootstrap\Interfaces\IConfig;

class ErrorHandler {


    private static $config;

    public static function setConfig(IConfig $config){
        self::$config = $config;
    }

    private static $errors = array(
        1       => 'E_FATAL',
        2       => 'E_WARNING',
        4       => 'E_EXCEPTION',
        8       => 'E_NOTICE',
        256     => 'E_USER_ERROR',
        512	    => 'E_USER_WARNING',
        1024    => 'E_USER_NOTICE',
        4096    => 'E_RECOVERABLE_ERROR',
        8191    => 'E_ALL'
    );

    public static function errorNumberToString($number){
        if(isset(self::$errors[$number])){
            return self::$errors[$number];
        }

        return "UnknownErrorType($number)";
    }


    private static function log(LogDetails $details){
        $line =
            $details->getTime() . ': ' .
            ErrorHandler::errorNumberToString($details->getNumber()) . ' - ' .
            $details->getMessage() . ' ' .
            $details->getFile() . '@' .
            $details->getLine() . PHP_EOL;


        file_put_contents(APP_DIR . '/' . self::$config->get('error', 'log'), $line, FILE_APPEND);
    }


    public static function handleError($errno, $errstr, $errfile, $errline){

        // if (!(error_reporting() & $errno)) {
            // This error code is not included in error_reporting
            // return;
        // }

        $details = new LogDetails();
        $details->setNumber($errno)->setMessage($errstr)->setFile($errfile)->setLine($errline);

        ErrorHandler::log($details);

        return true;
    }


    public static function handleFatal(){

        $errfile = "unknown file";
        $errstr  = "shutdown";
        $errno   = E_CORE_ERROR;
        $errline = 0;

        $error = error_get_last();

        if($error !== NULL){
            $errno   = $error["type"];
            $errfile = $error["file"];
            $errline = $error["line"];
            $errstr  = $error["message"];

            global $details;
            $details = new LogDetails();
            $details->setNumber($errno)->setMessage($errstr)->setFile($errfile)->setLine($errline);

            ErrorHandler::log($details);

            require_once APP_DIR . '/' . self::$config->get('error', 'page');
            exit(1);
        }
    }

    public static function handleException($ex){

        $errno   = 4;
        $errfile = $ex->getFile();
        $errline = $ex->getLine();
        $errstr  = $ex->getMessage();

        $details = new LogDetails();
        $details->setNumber($errno)->setMessage($errstr)->setFile($errfile)->setLine($errline);

        ErrorHandler::log($details);

        require_once APP_DIR . '/' . self::$config->get('error', 'page');
        exit(1);
    }


    public static function init(IConfig $config){

        self::setConfig($config);

        set_error_handler(array('TH\Bootstrap\ErrorHandler', 'handleError'));
        register_shutdown_function(array('TH\Bootstrap\ErrorHandler', 'handleFatal'));
        set_exception_handler(array('TH\Bootstrap\ErrorHandler', 'handleException'));
    }
}

class LogDetails {

    private $time;
    private $number;
    private $message;
    private $file;
    private $line;

    public function __construct(){
        $this->setTime(date('Y-m-d H:i:s'));
    }


    /**
     * Get the value of Time
     *
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set the value of Time
     *
     * @param mixed time
     *
     * @return self
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get the value of Number
     *
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set the value of Number
     *
     * @param mixed number
     *
     * @return self
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get the value of Message
     *
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set the value of Message
     *
     * @param mixed message
     *
     * @return self
     */
    public function setMessage($message)
    {

        $message = str_replace(array("\n", "\r"), '', $message);

        $this->message = $message;

        return $this;
    }

    /**
     * Get the value of File
     *
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set the value of File
     *
     * @param mixed file
     *
     * @return self
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get the value of Line
     *
     * @return mixed
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * Set the value of Line
     *
     * @param mixed line
     *
     * @return self
     */
    public function setLine($line)
    {
        $this->line = $line;

        return $this;
    }

}
