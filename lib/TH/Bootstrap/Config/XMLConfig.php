<?php
namespace TH\Bootstrap\Config;

use TH\Bootstrap\Interfaces\IConfig;
use TH\Bootstrap\Exception\NoFileException;
use TH\Bootstrap\Exception\NoKeyException;

class XMLConfig implements IConfig {

    private $xmlFile;

    public function __construct($filepath){
        if(!file_exists($filepath)){
            throw new NoFileException('Cannot find file "' . ($filepath) . '"');
        }

        $this->xmlFile = new \SimpleXMLElement(file_get_contents($filepath));
    }


    public function get(...$keys){
        $curr = $this->xmlFile;

        foreach($keys as $key){
            if($key[0] === '['){
                $key = (int) str_replace(array('[', ']'), '', $key);

                if(!isset($curr[$key])){
                    throw new NoKeyException('Cannot get key: ' . $key);
                }

                $curr = $curr[$key];
            } else {
                if(!isset($curr->$key)){
                    throw new NoKeyException('Cannot get key: ' . $key);
                }

                $curr = $curr->$key;
            }
        }

        if(count($curr) === 1){
            $curr = (string) $curr;
        }

        return $curr;
    }
}
