<?php
namespace TH\Bootstrap\Route;

class Route {

    private $uri;
    private $class;
    private $action;

    public function __construct($obj){
        $this->setUri($obj->uri)->setClass($obj->class);

        if(!isset($obj->action)){
            $obj->action = 'index';
        }

        $this->setAction($obj->action);
    }

    /**
     * Get the value of Uri
     *
     * @return mixed
     */
    public function getUri()
    {
        return (string) $this->uri;
    }

    /**
     * Set the value of Uri
     *
     * @param mixed uri
     *
     * @return self
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get the value of Class
     *
     * @return mixed
     */
    public function getClass()
    {
        return (string) $this->class;
    }

    /**
     * Set the value of Class
     *
     * @param mixed class
     *
     * @return self
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }


    /**
     * Get the value of Action
     *
     * @return mixed
     */
    public function getAction()
    {
        return (string) $this->action . 'Action';
    }

    /**
     * Set the value of Action
     *
     * @param mixed action
     *
     * @return self
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

}
