<?php
namespace TH\Bootstrap\Route;

use TH\Bootstrap\Config\XMLConfig;
use TH\Bootstrap\Interfaces\IConfig;
use TH\Bootstrap\Exception\NoRouteException;
use TH\Bootstrap\Route\Route;

class Router {

    private $routes;

    private static $instance;

    public static function getInstance(){
        if(self::$instance === null){
            self::$instance = new Router();
        }

        return self::$instance;
    }


    public function setConfig(IConfig $config){
        $this->routes = $config;
    }


    public function getErrorRoute(){
        $routes = $this->routes->get('route');

        foreach($routes as $route){
            $attributes = $route->attributes();


            if(isset($attributes['role']) && ((string) $attributes['role']) === 'error'){
                return new Route($route);
            }
        }

        throw new NoRouteException("Cannot find route with role='error'");
    }

    public function findRoute($uri){
        $routes = $this->routes->get('route');

        foreach($routes as $route){
            if(trim($route->uri) === trim($uri)){
                return new Route($route);
            }
        }

        throw new NoRouteException("Cannot find route for URI '$uri'");
    }


    public static function createFromFile($file){

        $xmlConfig = new XMLConfig($file);
        $router = Router::getInstance();
        $router->setConfig($xmlConfig);

        return $router;
    }


}
