<?php

require ('vendor/autoload.php');

//header('Content-Type: text/plain');

define('APPL_ENV', 'development');
define('APP_DIR', __DIR__);

use TH\Bootstrap\Runtime;
use TH\Bootstrap\Config\XMLConfig;

$applicationConfig = new XMLConfig('config/application.xml');
new Runtime($applicationConfig);


use TH\Bootstrap\Route\Router;
use TH\Bootstrap\Dispatcher;
$dispatcher = new Dispatcher(Router::getInstance());
$dispatcher->dispatch();
