<h1>Error!</h1>

<p>
    <?php
    $line =
        \TH\Bootstrap\ErrorHandler::errorNumberToString($details->getNumber()) . ' - ' .
        $details->getMessage() . ' ' .
        $details->getFile() . '@' .
        $details->getLine() . PHP_EOL;
    echo $line;

    ?>
</p>
