<?php
namespace App\Controller;

use TH\Bootstrap\Response\HTMLResponse;
use TH\Bootstrap\Response\JSONResponse;


class Home {


    public function indexAction(){
        return new HTMLResponse('home.twig.html', array('num' => rand(1, 10)));
    }


    public function jsonAction(){
        return new JSONResponse(array('num' => rand(1, 10)));
    }
}
