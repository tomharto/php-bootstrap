<?php
use PHPUnit\Framework\TestCase;
use TH\Bootstrap\Config\XMLConfig;
use TH\Bootstrap\Exception\NoFileException;
use TH\Bootstrap\Exception\NoKeyException;

require ('../vendor/autoload.php');

class XMLConfigTest extends TestCase
{

    public function setUp(){

    }

    public function testBadFile()
    {
        $this->expectException(NoFileException::class);
        $xmlConfig = new XMLConfig('config/routess.xml');
    }

    public function testGoodFile()
    {
        $xmlConfig = new XMLConfig('config/routes.xml');
    }

    public function testGetBadRoute(){
        $this->expectException(NoKeyException::class);
        $xmlConfig = new XMLConfig('config/routes.xml');
        $route = $xmlConfig->get('route', '[0]', 'badKey');
    }

    public function testGetGoodRoute(){
        $xmlConfig = new XMLConfig('config/routes.xml');
        $route = $xmlConfig->get('route', '[0]', 'uri');

        $this->assertEquals($route, '/');
    }
}
?>
