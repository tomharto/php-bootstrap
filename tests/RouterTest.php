<?php
use PHPUnit\Framework\TestCase;
use TH\Bootstrap\Route\Router;
use TH\Bootstrap\Route\Route;
use TH\Bootstrap\Exception\NoRouteException;

require ('../vendor/autoload.php');

class RouterTest extends TestCase
{


    public function errorHandler($errno, $errstr, $errfile, $errline){
        throw new \InvalidArgumentException(
            sprintf(
                'Missing argument. %s %s %s %s',
                $errno,
                $errstr,
                $errfile,
                $errline
            )
        );
    }

    public function testInitialization(){
        $router = Router::createFromFile('config/routes.xml');
    }

    public function testGetBadRoute(){
        $this->expectException(NoRouteException::class);
        $router = Router::createFromFile('config/routes.xml');
        $class = $router->findRoute('/somebadroute');
    }

    public function testGetGoodRoute(){
        $router = Router::createFromFile('config/routes.xml');
        $route = $router->findRoute('/');
        $this->assertEquals($route->getClass(), 'application\Controller\Home');
    }

    public function testBuildBadRoute(){
        $this->setExpectedException('\InvalidArgumentException');

        set_error_handler(array($this, 'errorHandler'));
        $data = array();
        $route = new Route($data);
    }

    public function testBuildGoodRoute(){
        $data = new stdClass;
        $data->uri = '/';
        $data->class = 'application\Controller\Home';

        $route = new Route($data);
        $this->assertEquals($route->getClass(), 'application\Controller\Home');
    }

}
?>
